import React from 'react';
import classNames from 'classnames';
import './Panel.css';

function Panel({ children, noShadow, bordered }) {
  const panelClasses = classNames(['panel'], {
    'panel--no-shadow': noShadow,
    'panel--bordered': bordered,
  });

  return (
    <div className={panelClasses}>
      <div className="panel__heading">Panel</div>
      {children}
    </div>
  );
}

export default Panel;
