import Panel from './components/Panel';
import './App.css';
import { useLayoutEffect } from 'react';

function App() {
  useLayoutEffect(() => {
    const theme = localStorage.getItem('theme');
    if (theme) {
      document.documentElement.dataset.theme = theme;
    }
  }, []);

  function toggleTheme() {
    const { theme } = document.documentElement.dataset;
    if (!theme) {
      document.documentElement.dataset.theme = 'dark';
      localStorage.setItem('theme', 'dark');
    } else {
      document.documentElement.dataset.theme = '';
      localStorage.setItem('theme', 'light');
    }
  }

  return (
    <>
      <Panel noShadow bordered>
        <h1>Hello, World!</h1>
      </Panel>
      <button onClick={toggleTheme}>Toggle theme</button>
    </>
  );
}

export default App;
